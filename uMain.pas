unit uMain;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  System.Threading,
  Vcl.StdCtrls,
  System.SyncObjs,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  Data.DB,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  FireDAC.Stan.Async,
  FireDAC.DApt,
  Resources.Conexao,
  System.Generics.Collections;

type
  TCommand = class;

  TViewMain = class(TForm)
    Button1: TButton;
    metProcessos: TFDMemTable;
    metProcessosid: TAutoIncField;
    metProcessosName: TStringField;
    metProcessosInstruction: TStringField;
    Metadata: TFDMetaInfoQuery;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    ltbTables: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button4Click(Sender: TObject);
  private
    FTaskList: TList<ITask>;
    FTaskCommandList: TList<ITask>;
    FThreadQueue: TThreadedQueue<TCommand>;
    FConnectionQueue: TThreadedQueue<TConexao>;
    FList: TThreadList<TCommand>;
    FMainTask: ITask;

    function AllThreadIsStoped: Boolean;
    procedure StopAllThreads();
    procedure StopAllExportsTasks();
    procedure DestruirObjetos;
  public
  end;

  TCommand = class(TObject)
  private
    FName: String;
    FSQL: String;
  public
    property Name: String read FName write FName;
    property SQL: String read FSQL write FSQL;
    constructor Create(const AName, ASQL: String); overload;
  end;

var
  ViewMain: TViewMain;

  threadvar TableName: String;

implementation

uses
  CodeSiteLogging,
  System.Json,
  System.IOUtils;

{$R *.dfm}

function IsNumericField(AField: TField): Boolean;
begin
  case AField.DataType of
    ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD, ftAutoInc, ftLargeint, ftFMTBcd, ftLongWord, ftShortint, ftByte,
      ftExtended, ftSingle:
      Result := True;
  else
    Result := False;
  end;

end;

function IsStringField(AField: TField): Boolean;
begin
  case AField.DataType of
    ftString, ftMemo, ftWideString, ftFixedWideChar, ftWideMemo, ftFixedChar:
      Result := True;
  else
    Result := False;
  end;

end;

function TViewMain.AllThreadIsStoped: Boolean;
var
  lTask: ITask;
begin
  for lTask in FTaskList do begin
    if lTask.Status = TTaskStatus.Running then
      Exit(False);
  end;
  Result := True;
end;

procedure TViewMain.Button1Click(Sender: TObject);
var
  LCount: Integer;
begin
  LCount := 0;
  TParallel.&For(1, 10,
    procedure(AIndex: Integer)
    begin
      TInterlocked.Add(LCount, AIndex);
    end);
  CodeSite.Send(csmLevel1, 'LCount', LCount);
end;

procedure TViewMain.Button2Click(Sender: TObject);
begin
  if (AllThreadIsStoped) then begin
    ShowMessage('Task is Read');
  end
  else
    ShowMessage('Task Is Running');
end;

procedure TViewMain.Button3Click(Sender: TObject);
begin
  StopAllThreads();
end;

procedure TViewMain.Button4Click(Sender: TObject);
var
  lTask: ITask;
  lItem: Integer;
begin
  if not(AllThreadIsStoped) then begin
    ShowMessage('Task Is Running');
    Exit;
  end;
  FThreadQueue := TThreadedQueue<TCommand>.Create();
  FConnectionQueue := TThreadedQueue<TConexao>.Create();
  for lItem := 1 to TThread.ProcessorCount do begin
    FConnectionQueue.PushItem(TConexao.Create(nil));
  end;

  metProcessos.First;
  while not metProcessos.Eof do begin
    // FThreadQueue.PushItem(TCommand.Create(metProcessosName.AsString, metProcessosInstruction.AsString));
    FList.Add(TCommand.Create(metProcessosName.AsString, metProcessosInstruction.AsString));
    metProcessos.Next;
    Application.ProcessMessages;
    // Application.ProcessMessages;
  end;

  FMainTask := TTask.Run(
    procedure
    var
      lListCommand: TList<TCommand>;
      lCommand: TCommand;
    begin
      lListCommand := FList.LockList;
      try
        for lCommand in lListCommand do begin
          TTask.CurrentTask.CheckCanceled;
          FThreadQueue.PushItem(TCommand.Create(lCommand.Name, lCommand.SQL));
          lTask := TTask.Run(
            procedure
            var
              lConexao: TConexao;
              lResultSet: TDataSet;
              lFieldIndex: Integer;
              lJSONArray: TJSONArray;
              lJSONData: TJSONObject;
              lInternalCommand: TCommand;
              lField: TField;
            begin
              lConexao := FConnectionQueue.PopItem;
              try
                lInternalCommand := FThreadQueue.PopItem;
                TThread.Queue(nil,
                  procedure
                  begin
                    ltbTables.Items.Add(lInternalCommand.Name);
                  end);

                lJSONArray := TJSONArray.Create;
                lConexao.FDConnection.ExecSQL(lInternalCommand.SQL, lResultSet);
                CodeSite.Send(csmLevel2, 'lCommand.SQL', lInternalCommand.SQL);
                CodeSite.Send(csmLevel2, 'lResultSet.ClassName', lResultSet.ClassName);
                CodeSite.Send(csmLevel2, 'lResultSet.RecordCount', lResultSet.RecordCount);
                lResultSet.First;
                while not lResultSet.Eof do begin
                  TTask.CurrentTask.CheckCanceled;
                  lJSONData := TJSONObject.Create;
                  for lFieldIndex := 0 to Pred(lResultSet.FieldCount) do begin
                    lField := lResultSet.Fields[lFieldIndex];
                    if (IsNumericField(lField)) then begin
                      lJSONData.AddPair(TJSONPair.Create(lField.FieldName, TJSONNumber.Create(lField.AsFloat)));
                    end;
                    if (IsStringField(lField)) then begin
                      lJSONData.AddPair(TJSONPair.Create(lField.FieldName, lField.AsString));
                    end;
                  end;
                  if (lJSONData.Count > 0) then begin
                    lJSONArray.Add(lJSONData);
                  end
                  else begin
                    lJSONData.Free;
                  end;

                  lResultSet.Next;
                end;
                if (lJSONArray.Count > 0) then begin
                  TThread.Sleep(100);
                  TFile.WriteAllText(String.Format('C:\tmp\ExportData\%s.json', [lInternalCommand.Name]), lJSONArray.ToJSON,
                    TEncoding.UTF8);
                  CodeSite.Send(csmLevel2, 'FileName', String.Format('C:\tmp\ExportData\%s.json', [lInternalCommand.Name]));
                end;
                lResultSet.Close;
                lResultSet.Free;
                lJSONArray.Free;
                lInternalCommand.Free;
              finally
                FConnectionQueue.PushItem(lConexao)
              end;
            end);
          FTaskCommandList.Add(lTask);
          if (FTaskCommandList.Count = 25) then begin
            TTask.WaitForAll(FTaskCommandList.ToArray);
            FTaskCommandList.Clear;
          end;

        end;
      finally
        FList.UnlockList;
      end;

    end);

  // TTask.Run(
  // procedure
  // begin
  // // ltbTables.Items.Add(metProcessosName.AsString);
  // while FThreadQueue.QueueSize > 0 do begin
  // TTask.CurrentTask.CheckCanceled;
  // lTask :=  TTask.Run(
  // procedure
  // var
  // lConexao: TConexao;
  // lResultSet: TDataSet;
  // lFieldIndex: Integer;
  // lJSONArray: TJSONArray;
  // lJSONData: TJSONObject;
  // lInternalCommand: TCommand;
  // lField: TField;
  // begin
  // lConexao := FConnectionQueue.PopItem;
  // try
  // lInternalCommand := FThreadQueue.PopItem;
  // lJSONArray := TJSONArray.Create;
  // lConexao.FDConnection.ExecSQL(lInternalCommand.SQL, lResultSet);
  // CodeSite.Send(csmLevel2, 'lCommand.SQL', lInternalCommand.SQL);
  // CodeSite.Send(csmLevel2, 'lResultSet.ClassName', lResultSet.ClassName);
  // CodeSite.Send(csmLevel2, 'lResultSet.RecordCount', lResultSet.RecordCount);
  // lResultSet.First;
  // while not lResultSet.Eof do begin
  // lJSONData := TJSONObject.Create;
  // for lFieldIndex := 0 to Pred(lResultSet.FieldCount) do begin
  // lField := lResultSet.Fields[lFieldIndex];
  // if (IsNumericField(lField)) then begin
  // lJSONData.AddPair(TJSONPair.Create(lField.FieldName, TJSONNumber.Create(lField.AsFloat)));
  // end;
  // if (IsStringField(lField)) then begin
  // lJSONData.AddPair(TJSONPair.Create(lField.FieldName, lField.AsString));
  // end;
  // end;
  // if (lJSONData.Count > 0) then begin
  // lJSONArray.Add(lJSONData);
  // end
  // else begin
  // lJSONData.Free;
  // end;
  //
  // lResultSet.Next;
  // end;
  // if (lJSONArray.Count > 0) then begin
  // TThread.Sleep(100);
  // TFile.WriteAllText(String.Format('C:\tmp\ExportData\%s.json', [lInternalCommand.Name]), lJSONArray.ToJSON, TEncoding.UTF8);
  // CodeSite.Send(csmLevel2, 'FileName', String.Format('C:\tmp\ExportData\%s.json', [lInternalCommand.Name]));
  // end;
  // lResultSet.Close;
  // lResultSet.Free;
  // lJSONArray.Free;
  // lInternalCommand.Free;
  // finally
  // FConnectionQueue.PushItem(lConexao)
  // end;
  // end);
  // FTaskCommandList.Add(lTask);
  // if (FTaskCommandList.Count = 20) then begin
  // TTask.WaitForAll(FTaskCommandList.ToArray);
  // FTaskCommandList.Clear;
  //
  // end;
  // end;
  //
  //
  // end);

  // if (FTaskCommandList.Count = 50) then begin
  // TTask.WaitForAll(FTaskCommandList.ToArray);
  // Application.ProcessMessages;
  // FTaskCommandList.Clear;
  // end;
  Application.ProcessMessages;

end;

procedure TViewMain.DestruirObjetos;
var
  lItem: TObject;
  lLista: TList<TCommand>;
  lIndex: Integer;
begin
  FTaskCommandList.Clear;
  FTaskList.Clear;

  FreeAndNil(FTaskCommandList);
  FreeAndNil(FTaskList);
  lLista := FList.LockList;
  try
    for lIndex := 0 to Pred(lLista.Count) do begin
      lLista.Items[lIndex].Free;
      lLista.Items[lIndex] := nil;
    end;
    lLista.Pack;
  finally
    FList.UnlockList;
  end;
  FreeAndNil(FList);

  if (FThreadQueue <> nil) then begin
    while FThreadQueue.QueueSize > 0 do begin
      lItem := FThreadQueue.PopItem();
      FreeAndNil(lItem);
    end;
    FreeAndNil(FThreadQueue);
  end;
  if (FConnectionQueue <> nil) then begin
    while FConnectionQueue.QueueSize > 0 do begin
      lItem := FConnectionQueue.PopItem();
      FreeAndNil(lItem);
    end;
    FreeAndNil(FConnectionQueue);
  end;
  FMainTask := nil;
end;

procedure TViewMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  StopAllThreads();
  StopAllExportsTasks();
  Sleep(2000);
  DestruirObjetos;
  metProcessos.EmptyDataSet;
end;

procedure TViewMain.FormCreate(Sender: TObject);
var
  lTask: ITask;
begin
  metProcessos.CreateDataSet;
  FTaskList := TList<ITask>.Create;
  FTaskCommandList := TList<ITask>.Create;
  FList := TThreadList<TCommand>.Create;
  lTask := TTask.Run(
    procedure
    var
      lMetadata: TFDMetaInfoQuery;
      lConexao: TConexao;
    begin
      lConexao := TConexao.Create(nil);
      lMetadata := TFDMetaInfoQuery.Create(nil);
      try
        lMetadata.Connection := lConexao.FDConnection;
        lMetadata.Transaction := lConexao.FDConnection.Transaction;
        lMetadata.UpdateTransaction := lConexao.FDConnection.UpdateTransaction;
        lMetadata.Open;
        lMetadata.First;
        while (not lMetadata.Eof) do begin
          metProcessos.Append;
          metProcessosName.AsString := lMetadata.FieldByName('TABLE_NAME').AsString;
          // TThread.Sleep(3000);
          metProcessosInstruction.AsString := String.Format('select first 20000 * from %s', [metProcessosName.AsString]);
          metProcessos.Post;
          if (TTask.CurrentTask.Status = TTaskStatus.Canceled) then begin
            CodeSite.Send(csmLevel1, 'Cancelled', TTask.CurrentTask.Id.ToString);
            Break;
          end;

          lMetadata.Next;
        end;
        lConexao.FDConnection.Close;
      finally
        FreeAndNil(lConexao);
        FreeAndNil(lMetadata);
      end;

    end);
  FTaskList.Add(lTask);
  // lTask := TTask.Run(
  // procedure
  // var
  // lMetadata: TFDMetaInfoQuery;
  // lConexao: TConexao;
  // begin
  // lConexao := TConexao.Create(Self);
  // lMetadata := TFDMetaInfoQuery.Create(Self);
  // try
  // lMetadata.Connection := lConexao.FDConnection;
  // lMetadata.Transaction := lConexao.FDConnection.Transaction;
  // lMetadata.UpdateTransaction := lConexao.FDConnection.UpdateTransaction;
  // lMetadata.Open;
  // lMetadata.First;
  // while (not lMetadata.Eof) and (lMetadata.RecNo <> 5) do begin
  // // TThread.Sleep(2000);
  // if (TTask.CurrentTask.Status = TTaskStatus.Canceled) then begin
  // CodeSite.Send(csmLevel1, 'Cancelled', TTask.CurrentTask.Id.ToString);
  // Break;
  // end;
  // lMetadata.Next;
  // end;
  // lConexao.FDConnection.Close;
  // finally
  // FreeAndNil(lConexao);
  // FreeAndNil(lMetadata);
  // end;
  // end);
  // FTaskList.Add(lTask);

end;

procedure TViewMain.StopAllExportsTasks;
// var
// lTask: ITask;
begin
  // for lTask in FTaskCommandList do begin
  // if lTask.Status = TTaskStatus.Running then begin
  // lTask.Cancel;
  // end;
  // end;
  if (FMainTask.Status = TTaskStatus.Running) then begin
    FMainTask.Cancel;
  end;

  // FMainTask.Wait();
end;

procedure TViewMain.StopAllThreads;
var
  lTask: ITask;
begin
  for lTask in FTaskList do begin
    if lTask.Status = TTaskStatus.Running then begin
      lTask.Cancel;
    end;
  end;
end;

{ TCommand }

constructor TCommand.Create(const AName, ASQL: String);
begin
  inherited Create();
  FName := AName;
  FSQL := ASQL;
end;

initialization

ReportMemoryLeaksOnShutdown := True;

end.
