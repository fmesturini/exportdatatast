object ViewMain: TViewMain
  Left = 0
  Top = 0
  Caption = 'Thread Demo'
  ClientHeight = 448
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 40
    Top = 24
    Width = 75
    Height = 25
    Caption = 'TInteloked'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 536
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Task Running'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 536
    Top = 55
    Width = 75
    Height = 25
    Caption = 'Stop Tasks'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 40
    Top = 72
    Width = 113
    Height = 25
    Caption = 'Start All Commands'
    TabOrder = 3
    OnClick = Button4Click
  end
  object ltbTables: TListBox
    Left = 40
    Top = 112
    Width = 553
    Height = 305
    ItemHeight = 13
    TabOrder = 4
  end
  object metProcessos: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 312
    Top = 152
    object metProcessosid: TAutoIncField
      FieldName = 'id'
    end
    object metProcessosName: TStringField
      FieldName = 'Name'
      Size = 120
    end
    object metProcessosInstruction: TStringField
      FieldName = 'Instruction'
      Size = 20000
    end
  end
  object Metadata: TFDMetaInfoQuery
    Connection = Conexao.DBErpsoft
    Left = 312
    Top = 56
  end
end
