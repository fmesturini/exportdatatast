unit Resources.Conexao;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Threading,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  Data.DB,
  FireDAC.Comp.Client,
  FireDAC.ConsoleUI.Wait,
  FireDAC.Phys.IBBase,
  FireDAC.Comp.UI;

type
  TConexao = class(TDataModule)
    DBErpsoft: TFDConnection;
    Cursor: TFDGUIxWaitCursor;
    Driver: TFDPhysFBDriverLink;
    procedure DataModuleCreate(Sender: TObject);
  private
    function GetFDConnection: TFDConnection;
    { Private declarations }
  public
    property FDConnection: TFDConnection read GetFDConnection;
  end;

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

procedure TConexao.DataModuleCreate(Sender: TObject);
// var
// Pool: TThreadPool;
begin
  // Pool := TThreadPool.Create;
  // Pool.SetMaxWorkerThreads(500);
  // Pool.SetMinWorkerThreads(200);
  // TTask.Run(Seld, EvNotifyEvent, Pool);
end;

function TConexao.GetFDConnection: TFDConnection;
begin
  Result := DBErpsoft;
end;

end.
