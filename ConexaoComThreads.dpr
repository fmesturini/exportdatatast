program ConexaoComThreads;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {ViewMain},
  Resources.Conexao in 'Resources.Conexao.pas' {Conexao: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TViewMain, ViewMain);
  Application.Run;
end.
